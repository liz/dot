#!/bin/bash

#   Copyright 2023 Elizabeth (4661) Cray
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source /etc/os-release

# Passwordless Sudo
echo "$(whoami)        ALL=(ALL:ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/nopw

# Packages
while read PKG; do
	sudo apt-get -qq -y install $PKG
done < Packages.lst

# Groups
GROUPS=(dialout floppy sudo root cdrom audio video plugdev users netdev bluetooth)
for GROUP in $GROUPS; do
	sudo usermod -aG $GROUP $(whoami)
done

# Default Configs
sudo update-alternatives --set editor /usr/bin/nvim
sudo update-alternatives --set vim /usr/bin/nvim

# Git Configs
git config --global user.name "Elizabeth Cray"
git config --global user.email "liz@cray.lgbt"
git config --global init.defaultBranch main

# Zsh + Home Dir
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
sudo chsh -s $(which zsh) $(whoami)
cp zsh.profile $HOME/.zshrc
mkdir -p $HOME/.bin
mkdir -p $HOME/.sbin
mkdir -p $HOME/.local/bin
mkdir -p $HOME/.local/sbin

# GUI Stuffs
if [[ "$(sudo systemctl is-active lightdm)" != *"inactive"* ]] || [[ "$(sudo systemctl is-active gdm3)" != *"inactive"* ]]; then
	# System running GUI
	while read PKG; do
		sudo apt-get -qq -y install $PKG
	done < Packages-gui.lst

	sudo update-alternatives --set x-terminal-emulator /usr/bin/gnome-terminal
	sudo update-alternatives --set www-browser /usr/bin/chromium
	sudo update-alternatives --set x-www-browser /usr/bin/chromium
fi

# Snap
while read PKG; do
	sudo snap install $PKG
done < Packages-snap.lst

# Flatpak
sudo flatpak remote-add flathub https://dl.flathub.org/repo/flathub.flatpakrepo
while read PKG; do
	sudo flatpak install -y --noninteractive --or-update $PKG
done < Packages-flatpak.lst

# Neovim Config
# nowrap, numbers, plugins
